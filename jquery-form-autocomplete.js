(function ( $ ) {
	$.fn.exists = function () {
		return this.length !== 0;
	}

	$.fn.form_autocomplete = function ( options ) {
		$.fn.form_autocomplete.defaults = {
		};
		var opts = merge_options();

		this.filter("form.form-autocomplete").each(function () {
			var current_form = $(this);

			current_form.attr('autocomplete', 'off');
			current_form.find('input').attr('autocomplete', 'off');
		});

		return this;
	};

	function merge_options() {
		var new_options = {};

		for (var key in $.fn.form_autocomplete.defaults) {
			var option = $.fn.form_autocomplete.defaults[key];
			if (option.constructor == Array) {
				new_options[key] = $.merge( $.merge([], options[key]), option );
			} else {
				if ( typeof options[key] === 'undefined' || options[key] === null ) {
					new_options[key] = option;
				} else {
					new_options[key] = options[key];
				}
			}
		}

		return new_options;
	}
}( jQuery ));
